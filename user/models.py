from django.contrib.auth.models import AbstractUser
from django.core.validators import RegexValidator
from django.db import models

from utils.models import UserModel

phone_regex = RegexValidator(
        regex=r'\+?1?\d{9,15}$',
        message='Formato de numero telefonico, incluido el codigo del pais.'
    )


class User(UserModel, AbstractUser):
    """
    Clase customizada de usuario django.
    Se reescribe el campo email.
    """
    email = models.EmailField(
        unique=True,
        error_messages= {'unique': 'Un usuario con ese email ya existe.'}
    )

    numero_telefono = models.CharField(validators=[phone_regex], max_length=17, blank=True)
    esta_verificado = models.BooleanField(
        default=False,
        help_text='Pone en verdadero cuando el usuario verifica su email'
    )

    esta_activo = models.BooleanField(
        default=True,
        help_text='Usuarios activo para el sistema'
    )

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'first_name', 'last_name']

    def __str__(self):
        return '{}, {}'.format(self.first_name.capitalize(), self.last_name.capitalize())

    def get_short_name(self):
        return self.first_name.capitalize()
