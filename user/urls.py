from django.urls import path, include
from rest_framework.routers import DefaultRouter

from user.views import UserViewSet, StatusViewSet

router = DefaultRouter()
router.register(r'usuarios', UserViewSet, basename='usuarios')
router.register(r'status', StatusViewSet, basename='status')

urlpatterns = [
    path('', include(router.urls))
]
