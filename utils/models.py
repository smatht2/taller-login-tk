from django.db import models


class UserModel(models.Model):
    creado = models.DateTimeField(
        auto_now_add=True,
        help_text=''
    )

    modificado = models.DateTimeField(null=True, blank=True)

    class Meta:
        abstract = True
        get_latest_by = 'creado'
        ordering = ['-creado', '-modificado']
